#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <setjmp.h>
#include "utils.h"

jmp_buf state; 

void sigfun(int s) {
	if (s == SIGALRM) {
		longjmp(state, 1);
	}
}

int main(int argc, char **argv) {
	/* colleghiamo SIGALRM alla nostra sigfun */
	signal(SIGALRM, sigfun);
	
	if (setjmp(state)) {
		/* se non zero, allora torno */
		zprintf(1, "[%d] waited enough!\n", getpid());
	} else {
		/* se zero, primo passaggio */
		alarm(10);
		zprintf(1, "[%d] timer activated!\n", getpid());
		
		for (;;) {
			pause();
		}
	}
	
	exit(0);
}	


