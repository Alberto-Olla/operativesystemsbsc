# Esercizi per il corso di Sistemi Operativi (DIEF, UNIMORE) #

## Come usare il materiale ##
Il materiale può essere scaricato usando git:

* $ git clone https://agresearch@bitbucket.org/agr_unimore/operativesystemsbsc.git

Il materiale può essere aggiornato in corso d'opera. Per ottenere la versione più recente, è sufficiente posizionarsi all'interno della cartella che contiene in materiale e digitare il comando:

* $ git pull

## Modulo 1 (Shell) ##
* Obiettivo: Guida all'installazione di Linux Ubuntu su macchina virtuale. Introduzione a vim e vimtutor. Uso della shell: console grafica e testuale. Comandi cd, pwd, ls, cp, mv, cat, grep; esempi di nomi assoluti e relativi; permessi di accesso ai file usando chmod e chown; man e la documentazione in linea. 

## Modulo 2 (Shell) ##
* Obiettivo: Struttura shell script: variabili $# e $n e costrutti true, false, test e case. Manipolazione file system: comandi mkdir, rm.

## Modulo 3 (Shell) ##
* Obiettivo: Ridirezione degli stream di caratteri. Variabile PATH. Controllo degli argomenti e ricerche ricorsive. Utilizzo del comando expr per eseguire calcoli aritmetici.

## Modulo 4 (Shell) ##
* Obiettivo: Svolgimento esercizi avanzati shell (ricerche ricorsive).

## Modulo 5 (Shell) ##
* Obiettivo: Svolgimento esercizi avanzati shell (ricerche ricorsive). Introduzione alle librerie Bash e all'utilizzo di funzioni.


## Modulo 6 (C) ##
* Obiettivo: Ripasso fondamenti linguaggio C. Funzioni read(), write(), open() e differenze rispetto a fread(), fwrite(), fopen(). Utilizzo di una semplice libreria. Introduzione alla compilazione con gcc e make.

## Modulo 7 (C) ##
* Obiettivo: Le funzioni fork() e wait() per generare e sincronizzare processi paralleli. Utilizzo indipendente e condiviso di file descriptors.

## Modulo 8 (C) ##
* Obiettivo: Comunicazione fra processi utilizzando la system call pipe(). Costruzione di diverse topologie di comunicazione e di gerarchie di processi, attraverso la configurazione dei lati di una pipe. Introduzione ad una scrittura sintetica del makefile.

## Modulo 09 (C) ##
* Obiettivo: Esercizi di riepilogo (prove d'esame svolte).

## Modulo 10 (C) ##
* Obiettivo: Comunicazione fra processi utilizzando i segnali. System calls signal(), alarm(), sleep(), pause(), utilizzo avanzato di wait(). Ripasso puntatori a funzione.