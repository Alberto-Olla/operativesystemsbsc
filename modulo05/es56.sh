#!/bin/bash

source oslib.sh

# Process a single directory
process_dir() {	
	match=N
	for fname in *; do
        if [ -f "$fname" -a -r "$fname" ]; then
        	lines=$(cat "$fname" | wc -l)
        	if [ $lines -eq $K ]; then
        		match=Y
        		sort "$fname" > "$fname".sort
        	fi
        fi
    done
    
    if [ $match == "Y" ]; then
    	echo $(pwd)
    	return 0
    fi
	
    return 1
}

# Loop folders recusively
recurse_dir() {	
    cd "$1"
    
    # Eventually notifies the name of current dir
    if [ "$DEBUG" == "ON" ]; then
		echo "# entering " `pwd`
    fi
    
    # Process current dir
    process_dir 
    
    # Scan for sub-directories
	for fname in *; do
        if [ -d "$fname" -a -x "$fname" ]; then
            recurse_dir "$fname" 
        fi
    done
    
    cd ..
}

# Check arguments
check_argc $# 2
check_dir_absolute "$1"
check_dir_explorable "$1"
check_number "$2"
check_number_positive_strict "$2"

# Variables
G="$1"
K="$2"
DEBUG=OFF

# Recursive exploration
recurse_dir "$G"

# Result
exit 0
