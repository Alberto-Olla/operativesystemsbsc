#!/bin/bash

source oslib.sh

# Process a single directory
process_dir() {
	if [ ! -r "$F.1" ]; then
		return 1
	fi
	
	if [ ! -r "$F.2" ]; then
		return 1
	fi
	
	size_f1=$(cat "$F.1" | wc -c)
	size_f2=$(cat "$F.2" | wc -c)
	line_f1=$(cat "$F.1" | wc -l)
	line_f2=$(cat "$F.2" | wc -l)
	
	if [ $size_f1 -eq $size_f2 -a $line_f1 -eq $line_f2 ]; then
		echo $(pwd)
	fi
		
    return 0
}

# Loop folders recusively
recurse_dir() {	
    cd "$1"
    
    # Eventually notifies the name of current dir
    if [ "$DEBUG" == "ON" ]; then
		echo "# entering " `pwd`
    fi
    
    # Process current dir
    process_dir 
    
    # Scan for sub-directories
	for fname in *; do
        if [ -d "$fname" -a -x "$fname" ]; then
            recurse_dir "$fname" 
        fi
    done
    
    cd ..
}

# Check arguments
check_argc $# 2
check_dir_absolute "$1"
check_dir_explorable "$1"

# Variables
G="$1"
F="$2"
DEBUG=OFF

# Recursive exploration
recurse_dir "$G"

# Result
exit 0
