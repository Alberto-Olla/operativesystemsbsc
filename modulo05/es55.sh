#!/bin/bash

source oslib.sh

# Process a single directory
process_dir() {
	for fname in *; do
        if [ -f "$fname" -a -r "$fname" ]; then
        
        	# check size
        	size=$(cat "$fname" | wc -c)
        	if [ $(expr $size % 2) != 0 ]; then
        		continue
        	fi
        	
        	# check content
        	char_missing=N
        	for c in $C; do
        		grep $c $fname >/dev/null 2>&1
        		if [ $? -ne 0 ]; then
        			char_missing=Y
        		fi	
        	done
        	
        	if [ $char_missing == "N" ]; then
        		echo $(pwd)/"$fname"
        	fi
        fi
    done
    return 0
}

# Loop folders recusively
recurse_dir() {	
    cd "$1"
    
    # Eventually notifies the name of current dir
    if [ "$DEBUG" == "ON" ]; then
		echo "# entering " `pwd`
    fi
    
    # Process current dir
    process_dir 
    
    # Scan for sub-directories
	for fname in *; do
        if [ -d "$fname" -a -x "$fname" ]; then
            recurse_dir "$fname" 
        fi
    done
    
    cd ..
}

# Check arguments
check_argc_min $# 2
check_dir_absolute "$1"
check_dir_explorable "$1"

# Variables
G="$1"
shift
C="$*"
DEBUG=OFF

# Recursive exploration
recurse_dir "$G"

# Result
exit 0
