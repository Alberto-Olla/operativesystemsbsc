#!/bin/bash

if [ $# -lt 2 ]; then
  echo "usage: "$0" F G1 ... Gn"
  exit 1
fi

# Variables assignment
F="$1"
shift

# Parameters checks
case "$F" in
	*/*)  echo "usage: "$0" F G1 ... Gn"
		  exit 1
		  ;;
	*)    ;; 
esac

for G in $*; do
  case "$G" in
	/*) ;;
	*) echo "usage: "$0" F G1 ... Gn"
	   exit 1
	   ;;
  esac

  if [ ! -d "$G" -o ! -x "$G" ]; then
	echo "usage: "$0" F G1 ... Gn"
	exit 1
  fi
done

# Recursive call
export PATH=$(pwd):$PATH
n_global=0
for G in $*; do
  es48r.sh "$G" "$F"
  n_dir=$?
  echo "files found in $G: $n_dir..."
  n_global=$(expr $n_global + $n_dir)
done
  
echo "files found globally: $n_global!. bye!"
exit 0

