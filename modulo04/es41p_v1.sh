#!/bin/bash

G=$1

if [ $# -ne 1 ]; then
  echo "usage: "$0" dirname (absolute)"
  exit 1
fi

case "$G" in
  /*) ;;
  *)  echo "usage: "$0" dirname absolute"
      exit 1
      ;;
esac

if [ ! -d "$G" -o ! -x "$G" ]; then
  echo "usage: "$0" dirname (absolute)"
  exit 1
fi

echo "0" > /tmp/maxlevel
export PATH=$(pwd):$PATH
es41r_v1.sh "$G" 0
echo "Max depth reached: $(cat /tmp/maxlevel). Bye!"

exit 0
