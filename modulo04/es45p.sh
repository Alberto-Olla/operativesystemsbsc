#!/bin/bash

if [ $# -ne 2 ]; then
  echo "usage: "$0" G F"
  exit 1
fi

# Variables assignment
G="$1"
F="$2"

# Parameters checks
case "$G" in
	/*) ;;
	*) echo "usage: "$0" G F"
	   exit 1
	   ;;
esac

if [ ! -d "$G" -o ! -x "$G" ]; then
	echo "usage: "$0" G F"
	exit 1
fi

# Recursive call
export PATH=$(pwd):$PATH
es45r.sh "$G" "$F"

exit 0

