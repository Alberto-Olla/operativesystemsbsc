#!/bin/bash

cd "$1"

# Variables assignment
Cx="$2"
Cy="$3"

# Process directory
file_list=""
for fname in *; do
	if [ ! -f "$fname" -o ! -r "$fname" ]; then
	  continue
    fi
		
	case "$fname" in
	  *$Cx*$Cy*) name_check=0
			     ;;
	  *$Cy*$Cx*) name_check=0
			     ;;
	  *) name_check=1
		 ;;
	esac
	
	# avoid resource waste if name does not match
	# grepping is slow!
	if [ $name_check == 1 ]; then
	  continue
	fi
		
	grep -q $Cx "$fname"
	foundCx=$?
		
	grep -q $Cy "$fname"
	foundCy=$?
		
	if [ $name_check -eq 0 -a $foundCx -eq 0 -a $foundCy -eq 0 ]; then
	  echo "$(pwd) matches. Adding to list..."
	  file_list="$file_list "$fname""
	fi
done

# recursive cycle
for fname in *; do
  if [ -d "$fname" -a -x "$fname" ]; then
    $0 "$fname" "$Cx" "$Cy"
  fi
done

