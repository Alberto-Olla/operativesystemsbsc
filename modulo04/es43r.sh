#!/bin/bash

cd "$1"

# Process directory
n=0
file_list=""
file_last=""

for fname in *; do
	if [ -f "$fname" -a -r "$fname" ]; then
		n=$(expr $n + 1)
		file_list="$file_list "$fname""
		file_last="$fname"
	fi
done

if [ $n -ge 1 -a $n -le $2 ]; then
	echo "$(pwd) matched. last file seen: "$file_last"..."
	echo "$file_list" >> $3
fi

# recursive cycle
for fname in *; do
	if [ -d "$fname" -a -x "$fname" ]; then
		$0 "$fname" "$2" "$3"
	fi
done


