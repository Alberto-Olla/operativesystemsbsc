#!/bin/bash

# $1 contains a directory
# $2 contains the current depth

# enter dir and print some stats
cd "$1"
echo "[$2] $(pwd)"

# eventually update max level
if [ $2 -gt $(cat /tmp/maxlevel) ]; then
  echo "$2" > /tmp/maxlevel
fi

# recursive cycle
for file in *; do
  if [ -d "$file" -a -x "$file" ]; then
    $0 "$file" $(expr $2 + 1)
  fi
done

exit 
