#!/bin/bash

# $1 contains a directory
# $2 contains the current depth

# enter dir and print some stats
cd "$1"
echo "[$2] $(pwd)"

# recursive cycle
max_level=$2
for file in *; do
  if [ -d "$file" -a -x "$file" ]; then
    $0 "$file" $(expr $2 + 1)
    tmp_level=$?
    if [ $tmp_level -gt $max_level ]; then
      max_level=$tmp_level
    fi
  fi
done

exit $max_level
