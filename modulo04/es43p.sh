#!/bin/bash

if [ $# -ne 2 ]; then
  echo "usage: "$0" G K"
  exit 1
fi

# Variables assignment
G="$1"
K="$2"

# Parameters checks
case "$G" in
	/*) ;;
	*) echo "usage: "$0" G K"
	   exit 1
	   ;;
esac

if [ ! -d "$G" -o ! -x "$G" ]; then
	echo "usage: "$0" G K"
	exit 1
fi

expr "$K" + 0 > /dev/null 2>&1
case $? in
	0) ;; # risultato != 0
	1) ;; # risultato == 0
	2) echo "K must be a number"; exit 1;; # syntax error, es. K='+'
	3) echo "K must be a number"; exit 1;; # non-numeric argument, es. K='a'
esac

if [ $K -le 0 ]; then
	echo "K must be > 0"
	exit 1
fi

if [ $(expr $K % 2) -ne 0 ]; then
	echo "K must be even"
	exit 1
fi

ftemp=/tmp/temp$$
rm -f "$ftemp"
touch "$ftemp"

# Recursive call
export PATH=$(pwd):$PATH
es43r.sh "$G" "$K" "$ftemp"

#rm -f "$ftemp"

exit 0