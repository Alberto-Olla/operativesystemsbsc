#!/bin/bash

if [ $# -ne 3 ]; then
  echo "usage: "$0" G D N"
  exit 1
fi

# Variables assignment
G="$1"
D="$2"
N="$3"

# Parameters checks
case "$G" in
	/*) ;;
	*) echo "usage: "$0" G D N"
	   exit 1
	   ;;
esac

if [ ! -d "$G" -o ! -x "$G" ]; then
	echo "usage: "$0" G D N"
	exit 1
fi

case "$D" in
	*/*)  echo "usage: "$0" G D N"; 
		  exit 1
		  ;;
	*)    ;; 
esac

expr "$N" + 0 > /dev/null 2>&1
case $? in
	0) ;; # risultato != 0
	1) ;; # risultato == 0
	2) echo "N must be a number"; exit 1;; # syntax error, es. K='+'
	3) echo "N must be a number"; exit 1;; # non-numeric argument, es. K='a'
esac

if [ $N -le 0 ]; then
	echo "N must be > 0"
	exit 1
fi

# Recursive call
export PATH=$(pwd):$PATH
es44r.sh "$G" "$D" "$N" 
echo "found $? files! bye."

exit 0