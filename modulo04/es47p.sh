#!/bin/bash

if [ $# -lt 2 ]; then
  echo "usage: "$0" G C1 .. Cn"
  exit 1
fi

# Variables assignment
G="$1"
shift

# Parameters checks
case "$G" in
	/*) ;;
	*) echo "usage: "$0" G C1 .. Cn"
	   exit 1
	   ;;
esac

if [ ! -d "$G" -o ! -x "$G" ]; then
	echo "usage: "$0" G C1 .. Cn"
	exit 1
fi

for Cx in $*; do
	case "$Cx" in
		?) ;;
		*) echo ""$Cx" must be a char"
		   exit 1
		   ;;
	esac
done

# Recursive call
export PATH=$(pwd):$PATH
es47r.sh "$G" $*

exit 0
