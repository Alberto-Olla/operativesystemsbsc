#!/bin/bash

cd "$1"

# discard directory parameter
# it is not needed for recursion
shift

# Process directory
has_all=1
for fname in *; do
	if [ -f "$fname" -a -r "$fname" ]; then
		size=$(cat "$fname" | wc -c)
		for Cx in $*; do
			grep -q $Cx "$fname"
			if [ $? -ne 0 ]; then
				has_all=0
			fi
		done
		
		if [ $(expr $size % 2) -eq 0 -a $has_all -eq 1 ]; then
			echo "[found] $(pwd)/$fname"
		fi
	fi
done

# recursive cycle
for fname in *; do
	if [ -d "$fname" -a -x "$fname" ]; then
		$0 "$fname" "$*"
	fi
done

