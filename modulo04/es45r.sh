#!/bin/bash

cd "$1"

# Variables assignment
F="$2"

# Process directory
f1_found=0
if [ -f "$F.1" -a -r "$F.1" ]; then
  f1_found=1
  f1_lines=$(cat "$F.1" | wc -l)
  f1_bytes=$(cat "$F.1" | wc -c)
fi

f2_found=0
if [ -f "$F.2" -a -r "$F.2" ]; then
  f2_found=1
  f2_lines=$(cat "$F.2" | wc -l)
  f2_bytes=$(cat "$F.2" | wc -c)
fi

if [ $f1_found -eq 1 -a $f2_found -eq 1 ]; then
  if [ $f1_lines == $f2_lines -a $f1_bytes == $f2_bytes ]; then
    echo "[found] $(pwd)"
  fi
fi

# recursive cycle
for fname in *; do
	if [ -d "$fname" -a -x "$fname" ]; then
		$0 "$fname" "$F"
	fi
done
