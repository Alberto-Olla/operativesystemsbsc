#!/bin/bash

G=$1

if [ $# -ne 1 ]; then
  echo "usage: "$0" dirname (absolute)"
  exit 1
fi

case "$G" in
  /*) ;;
  *)  echo "usage: "$0" dirname absolute"
      exit 1
      ;;
esac

if [ ! -d "$G" -o ! -x "$G" ]; then
  echo "usage: "$0" dirname (absolute)"
  exit 1
fi

export PATH=$(pwd):$PATH
es41r_v2.sh "$G" 0
echo "Max depth reached: $?. Bye!"

exit 0
