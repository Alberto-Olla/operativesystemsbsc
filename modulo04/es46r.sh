#!/bin/bash

cd "$1"

# Variables assignment
D="$2"

# Process directory
name_ok=0
if [ $(basename $(pwd)) == "$D" ]; then
  name_ok=1
fi

n_dir=0
n_file=0
n_created=0
for fname in *; do
	if [ -f "$fname"  ]; then
		n_file=$(expr $n_file + 1)
	fi
	
	if [ -d "$fname"  ]; then
		n_dir=$(expr $n_dir + 1)
	fi
done

if [ $n_file -eq $(expr $n_dir + 2) -a $name_ok -eq 1 ]; then
	echo "[found] $(pwd)"
	
	while [ $n_file -gt $n_dir ]; do
		echo -n "Insert new dir name: "
		read dir_name
		mkdir "$dir_name"
		if [ $? -eq 0 ]; then
			n_dir=$(expr $n_dir + 1)
			n_created=$(expr $n_created + 1)
		fi
	done
fi

# recursive cycle
for fname in *; do
	if [ -d "$fname" -a -x "$fname" ]; then
		$0 "$fname" "$D"
		n_created=$(expr $n_created + $?)
	fi
done

exit $n_created

