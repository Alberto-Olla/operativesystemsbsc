#!/bin/bash

cd "$1"

# Variables assignment
D="$2"
N="$3"

# Process directory
name_ok=0
if [ $(basename $(pwd)) == "$D" ]; then
  name_ok=1
fi

n=0
list=""
for fname in *; do
	if [ -f "$fname" -a -r "$fname" ]; then
		fsize=$(cat "$fname" | wc -c)
		if [ $fsize -gt $N ]; then
			n=$(expr $n + 1)
			list=""$list" "$fname""
		fi
	fi
done

if [ $n -ge 1 -a $name_ok -eq 1 ]; then
	echo "[found] $(pwd)"
	cat $list > SOMMA
fi

# recursive cycle
for fname in *; do
	if [ -d "$fname" -a -x "$fname" ]; then
		$0 "$fname" "$D" "$N"
		n=$(expr $n + $?)
	fi
done

exit $n

