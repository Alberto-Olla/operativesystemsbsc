#!/bin/bash

if [ $# -ne 3 ]; then
  echo "usage: "$0" dirname Cx Cy"
  exit 1
fi

# Variables assignment
G="$1"
Cx="$2"
Cy="$3"

# Parameters checks
case "$G" in
	/*) ;;
	*) echo "usage: "$0" dirname Cx Cy"
	   exit 1
	   ;;
esac

if [ ! -d "$G" -o ! -x "$G" ]; then
	echo "usage: "$0" dirname Cx Cy"
	exit 1
fi

case "$Cx" in
	[0-9]) ;;
	*) echo "usage: "$0" dirname Cx Cy"
	   exit 1
	   ;;
esac

case "$Cy" in
	[0-9]) ;;
	*) echo "usage: "$0" dirname Cx Cy"
	   exit 1
	   ;;
esac

# Recursive call
export PATH=$(pwd):$PATH
es42r.sh $G $Cx $Cy

exit 0