#!/bin/bash

cd "$1"

# Variables assignment
G="$1"
F="$2"

# Process directory
n=0
if [ -f "$F" -a -r "$F" ]; then
  echo "[found] $(pwd)/"$F""
  sort "$F" > sorted
  n=$(expr $n + 1)
fi 
    
# recursive cycle
for fname in *; do
	if [ -d "$fname" -a -x "$fname" ]; then
		$0 "$fname" "$F"
		n=$(expr $n + $?)
	fi
done

exit $n


