#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include "utils.h"

int main(int argc, char **argv) {
	pid_t pid;
	int status;
	
	pid = fork();
	switch (pid) {
		case 0: /* child */
			zprintf(1, "[%d] Hello father! [fork() %d]\n", getpid(), pid);
			exit(0);
		case -1: /* error */
			zprintf(2, "error: fork()\n");
			exit(1);
		default: /* father */
			zprintf(1, "[%d] Hello child! [fork() %d]\n", getpid(), pid);
			wait(&status);
			zprintf(1, "[%d] Child exited %d\n", getpid(), WEXITSTATUS(status));
			exit(0);
	}
}


