#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "utils.h"

int main(int argc, char **argv) {
	int fd;
	
	if (argc < 2) {
		zprintf(2, "error: one parameter required\n");
		exit(1);
	} 
	
	fd = open(argv[1], O_WRONLY|O_CREAT|O_EXCL, 00666);
	if (fd < 0) {
		zprintf(2, "error: open()\n");
		exit(1);
	}
	
	if (close(fd) !=0) {
		zprintf(2, "error: close()\n");
		exit(1);
	}
	
	exit(0);
}

