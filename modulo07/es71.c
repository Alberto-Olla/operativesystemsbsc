#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char **argv) {
	char buffer[1024];
	int fdout = 1;
	int opt;

	for (;;) {
		opt = getopt(argc, argv, "f:");
		if (opt == -1) break;
		switch (opt) {
			case 'f':
				if (strcmp(optarg, "2") == 0) fdout = 2;
				break;
		}
	}

	sprintf(buffer, "Hello World [%d]!\n", fdout);
	write(fdout, buffer, strlen(buffer)); 
	exit(0);
}
