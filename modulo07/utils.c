#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>

/*
 * Formatted write() funtion 
 * Useful for writing compact low-lowel code 
 * using write() instead of printf()
 * Example: zprintf(1, "argc = %d\n", argc);
 */
void zprintf(int fd, const char *fmt, ...) {
	static char msg[1024];
	int n;
	va_list ap;
	
	va_start(ap, fmt);
	n = vsnprintf(msg, 1024, fmt, ap);
	write(fd,msg,n);
	va_end(ap);
}

