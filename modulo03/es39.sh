#!/bin/bash

F=0
D=0
for fname in *; do
  if [ "$fname" == "*" ]; then
    echo "error: empty directory"
    exit 1
  fi
  
  if [ -f "$fname" ]; then
    echo [F] "$fname"
    F=$(expr $F + 1)
  fi
  
  if [ -d "$fname" ]; then
    echo [D] "$fname"
    D=$(expr $D + 1)
  fi
done

echo "Files =" $F
echo "Directories =" $D
exit 0