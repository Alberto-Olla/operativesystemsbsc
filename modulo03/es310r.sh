#!/bin/sh
# Soluzione prova del 7 luglio 2009 - ricerca
# Dati: $1->directory da esplorare $2->K
cd $1

# Ricerca directory che soddisfano la richiesta
contafile=0
lista=""
for filename in *; do
	if [ -f $filename -a -r $filename ]; then
		contafile=`expr $contafile + 1`
		lista="$lista $filename"
	fi
done

# Controllo su contafile
if [ $contafile -ge $2 -a `expr $contafile % 2` -eq 0 ]; then
	echo Trovata directory `pwd` con $contafile file
	parteC $lista
fi

# Invocazione ricorsiva
for filename in *; do
	if [ -d $filename -a -x $filename ]; then
		$0 $filename $2
	fi
done

