#!/bin/bash

for fname in *; do
  cat "$fname" | grep "#!/bin/sh" >/dev/null 2>&1
  if [ $? -eq 1 ]; then
    echo "$fname"
  fi
done
