#!/bin/sh
# Soluzione prova del 7 luglio 2009 - principale
# Dati: $1->G $2->K

# Controllo argomenti ==2
case $# in
	2) ;;
	*) echo Uso: $0 G K; exit 1;;
esac

# Assegnamento nomi
G="$1"
K="$2"

# Controllo G assoluto
case "$G" in
	/*) ;;
	*) echo G non assoluto: "$G"; exit 1;;
esac

# Controllo G directory esistente, leggibile ed esplorabile
if [ ! -d "$G" -o ! -x "$G" ]
then
	echo G non directory esplorabile
	exit 1
fi

# Controllo K numerico
expr "$K" + 0 > /dev/null 2>&1
case $? in
	0) ;; # risultato != 0
	1) ;; # risultato == 0
	2) echo K non numerico: "$K"; exit 1;; # syntax error, es. K='+'
	3) echo K non numerico: "$K"; exit 1;; # non-numeric argument, es. K='a'
esac

# Controllo K strettamente positivo
if [ $K -le 0 ]
then
	echo K non strettamente positivo: "$K"
	exit 1
fi

# Controllo K pari
if [ `expr $K % 2` -ne 0 ]
then
	echo K non pari: "$K"
	exit 1
fi

# Predisposizione esecuzione
export PATH=`pwd`:$PATH

# Inizio ricerca
es47r.sh $G $K

