#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv) {
	char c;
	int lines = 0, fdin = 0, fdout = 1, n = 10;
	int opt;

	for (;;) {
		opt = getopt(argc, argv, "n:f:");
		if (opt == -1) break;
		switch (opt) {
			case 'n':
				n = atoi(optarg);
				break;
			case 'f':
				fdin = open(optarg, O_RDONLY);
				break;
		}
	}

	while(read(fdin, &c, 1) > 0) {
		if (lines < n) {
			write(fdout, &c, 1);
		}
		if (c == '\n') {
			lines++;
		}
	}     
	exit(0);
}
